import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { SeedService } from './seed.service';

@Resolver()
export class SeedResolver {
  constructor(private readonly seedService: SeedService) {}

  @Mutation(() => Boolean, {name: 'executeSeed', description: 'Create data base invoices.'})
  executeSeed() {
    return this.seedService.executeSeed();
  }

}
