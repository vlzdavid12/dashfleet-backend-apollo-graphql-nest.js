import { Module } from '@nestjs/common';
import { SeedService } from './seed.service';
import { SeedResolver } from './seed.resolver';
import {InvoiceModule} from "../invoice/invoice.module";
import {ConfigModule} from "@nestjs/config";

@Module({
  providers: [SeedResolver, SeedService],
  imports: [
      ConfigModule,
      InvoiceModule
  ]
})
export class SeedModule {}
