import {Injectable} from '@nestjs/common';
import {Invoice} from "../invoice/entities/invoice.entity";
import {Repository} from "typeorm";
import {SEED_INVOICES} from "./data/seed-data";
import {InjectRepository} from "@nestjs/typeorm";
import {InvoiceService} from "../invoice/invoice.service";

@Injectable()
export class SeedService {

    constructor(
        @InjectRepository(Invoice)
        private readonly invoiceRepository: Repository<Invoice>,
        private readonly  invoiceService: InvoiceService

        ) {
    }
    async executeSeed() {
        //Delete Databases
        await  this.deleteInvoices();

        //Create Invoices
        await this.loadInvoices();

        return true;
    }

    async deleteInvoices(){
        await this.invoiceRepository.createQueryBuilder()
            .delete()
            .where({})
            .execute()
    }

    async loadInvoices(): Promise<Invoice> {
        const invoices = [];
        for (const invoice of SEED_INVOICES) {
            try{
                let resp = await this.invoiceService.create(invoice)
                invoices.push(resp);
            }catch (error){
                console.log(error);
            }
        }
        return invoices[0];
    }
}

