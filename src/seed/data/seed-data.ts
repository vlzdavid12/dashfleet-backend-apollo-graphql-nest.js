function numberDocumentRandom(yourNumber): number {
    let text = "";
    let possible = "0123456789";

    for (let i = 0; i < yourNumber; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return Number(text);
}

function codeRequestRandom(yourNumber): string {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTVWXYZ0123456789";

    for (let i = 0; i < yourNumber; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}


export const SEED_INVOICES = [{
    "fullName": "david valenzuela",
    "codeRequest": codeRequestRandom(16),
    "typeDocument": "a",
    "numberDocument": numberDocumentRandom(8),
    "address": "cra 45 no 90 -89",
    "dateRequest": "2023-02-18",
    "status": true
},
    {
        "fullName": "adriana garcia",
        "codeRequest": codeRequestRandom(16),
        "typeDocument": "b",
        "numberDocument": numberDocumentRandom(8),
        "address": "cra 45 no 90 -89",
        "dateRequest": "2023-02-18",
        "status": true
    },
    {
        "fullName": "eduardo pardo",
        "codeRequest": codeRequestRandom(16),
        "typeDocument": "b",
        "numberDocument": numberDocumentRandom(8),
        "address": "cra 45 no 90 -89",
        "dateRequest": "2023-01-16",
        "status": true
    },
    {
        "fullName": "estaban guzman",
        "codeRequest": codeRequestRandom(16),
        "typeDocument": "a",
        "numberDocument": numberDocumentRandom(8),
        "address": "cra 45 no 90 -89",
        "dateRequest": "2023-05-20",
        "status": true
    },
    {
        "fullName": "juan estupiñan",
        "codeRequest": codeRequestRandom(16),
        "typeDocument": "c",
        "numberDocument": numberDocumentRandom(8),
        "address": "cra 45 no 90 -89",
        "dateRequest": "2023-05-20",
        "status": true
    },
    {
        "fullName": "federico martinez",
        "codeRequest": codeRequestRandom(16),
        "typeDocument": "c",
        "numberDocument": numberDocumentRandom(8),
        "address": "cra 45 no 90 -89",
        "dateRequest": "2023-05-20",
        "status": true
    },
    {
        "fullName": "alejandra garzon",
        "codeRequest": codeRequestRandom(16),
        "typeDocument": "d",
        "numberDocument": numberDocumentRandom(8),
        "address": "cra 45 no 90 -89",
        "dateRequest": "2023-05-20",
        "status": true
    },
    {
        "fullName": "claudia portugal",
        "codeRequest": codeRequestRandom(16),
        "typeDocument": "c",
        "numberDocument": numberDocumentRandom(8),
        "address": "cra 45 no 90 -89",
        "dateRequest": "2023-05-20",
        "status": true
    },
    {
        "fullName": "pedro gustamante",
        "codeRequest": codeRequestRandom(16),
        "typeDocument": "a",
        "numberDocument": numberDocumentRandom(8),
        "address": "cra 45 no 90 -89",
        "dateRequest": "2023-05-20",
        "status": true
    },
    {
        "fullName": "sandra garcia",
        "codeRequest": codeRequestRandom(16),
        "typeDocument": "b",
        "numberDocument": numberDocumentRandom(8),
        "address": "cra 45 no 90 -89",
        "dateRequest": "2023-05-20",
        "status": true
    },
    {
        "fullName": "stefani perdomo",
        "codeRequest": codeRequestRandom(16),
        "typeDocument": "c",
        "numberDocument": numberDocumentRandom(8),
        "address": "cra 45 no 90 -89",
        "dateRequest": "2023-05-20",
        "status": true
    },
    {
        "fullName": "monica giraldo",
        "codeRequest": codeRequestRandom(16),
        "typeDocument": "d",
        "numberDocument": numberDocumentRandom(8),
        "address": "cra 45 no 90 -89",
        "dateRequest": "2023-05-20",
        "status": true
    },
    {
        "fullName": "jhon santos",
        "codeRequest": codeRequestRandom(16),
        "typeDocument": "a",
        "numberDocument": numberDocumentRandom(8),
        "address": "cra 45 no 90 -89",
        "dateRequest": "2023-05-20",
        "status": true
    },
    {
        "fullName": "carlos restrepo",
        "codeRequest": codeRequestRandom(16),
        "typeDocument": "d",
        "numberDocument": numberDocumentRandom(8),
        "address": "cra 45 no 90 -89",
        "dateRequest": "2023-05-20",
        "status": true
    }
]
