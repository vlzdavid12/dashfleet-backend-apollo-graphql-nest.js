import {join} from 'path';
import { Module } from '@nestjs/common';
import {ConfigModule} from '@nestjs/config';



import {GraphQLModule} from "@nestjs/graphql";
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { InvoiceModule } from './invoice/invoice.module';
import {TypeOrmModule} from "@nestjs/typeorm";
import { SeedModule } from './seed/seed.module';
@Module({
  imports: [
      ConfigModule.forRoot(),
      GraphQLModule.forRoot<ApolloDriverConfig>({
          driver: ApolloDriver,
          playground: true,
          autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
          cors: {
              origin: "*",
              credentials: true
          }
      }),

      TypeOrmModule.forRoot({
          type: 'postgres',
          host: process.env.DB_HOST,
          port: Number(process.env.DB_PORT),
          username: process.env.DB_USERNAME,
          password: process.env.DB_PASSWORD,
          database: process.env.DB_NAME,
          synchronize: true,
          autoLoadEntities: true,
      }),

      InvoiceModule,

      SeedModule,


  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
