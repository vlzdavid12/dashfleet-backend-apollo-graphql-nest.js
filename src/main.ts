import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {ValidationPipe} from "@nestjs/common";

async function bootstrap() {
    const app = await NestFactory.create(AppModule,{cors: true});

    app.useGlobalPipes(
        new ValidationPipe({
            whitelist: true
        })
    );

    app.enableCors({
        origin: "*"
    });

    const PORT = process.env.PORT || 4000;
    await app.listen(PORT);
    console.log(`App running on port ${PORT}`)
}

bootstrap();
