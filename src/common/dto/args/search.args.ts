import {ArgsType, Field} from "@nestjs/graphql";
import {IsArray, IsOptional} from "class-validator";

@ArgsType()
export class SearchArgs {
    @Field(() => [String], {nullable: true})
    @IsOptional()
    @IsArray()
    search?: string[];
}
