import { InputType, Int, Field } from '@nestjs/graphql';
import {IsNotEmpty} from "class-validator";

@InputType()
export class CreateInvoiceInput {
  @Field(() => String)
  @IsNotEmpty()
  fullName: string;

  @Field(() => String)
  @IsNotEmpty()
  codeRequest: string

  @Field(() => String)
  @IsNotEmpty()
  typeDocument: string

  @Field(() => Int)
  @IsNotEmpty()
  numberDocument: number;

  @Field(() => String)
  @IsNotEmpty()
  dateRequest: string

  @Field(() => String)
  @IsNotEmpty()
  address: string

  @Field(() => Boolean)
  @IsNotEmpty()
  status: boolean

}
