import { CreateInvoiceInput } from './create-invoice.input';
import {InputType, Field, Int, PartialType, ID} from '@nestjs/graphql';
import {IsBoolean, IsNotEmpty, IsOptional, IsUUID} from "class-validator";

@InputType()
export class UpdateInvoiceInput extends PartialType(CreateInvoiceInput) {
  @Field(() => ID)
  @IsUUID()
  id: string;

  @Field(()=> String,{nullable: true})
  @IsOptional()
  fullName: string

  @Field(()=> String,{nullable: true})
  @IsOptional()
  codeRequest: string

  @Field(()=> String,{nullable: true})
  @IsOptional()
  typeDocument: string

  @Field(()=> Int,{nullable: true})
  @IsOptional()
  numberDocument: number

  @Field(() => String)
  @IsNotEmpty()
  address: string

  @Field(()=> String,{nullable: true})
  @IsOptional()
  dateRequest: string

  @Field(()=> Boolean,{nullable: true})
  @IsBoolean()
  @IsOptional()
  status: boolean
}
