import { Module } from '@nestjs/common';
import { InvoiceService } from './invoice.service';
import { InvoiceResolver } from './invoice.resolver';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Invoice} from "./entities/invoice.entity";

@Module({
  providers: [InvoiceResolver, InvoiceService],
  imports: [
      TypeOrmModule.forFeature([Invoice])
  ],
  exports: [
      TypeOrmModule,
      InvoiceService
  ]
})
export class InvoiceModule {}
