import {Resolver, Query, Mutation, Args, Int, ID} from '@nestjs/graphql';
import { InvoiceService } from './invoice.service';
import { Invoice } from './entities/invoice.entity';
import { CreateInvoiceInput } from './dto/create-invoice.input';
import { UpdateInvoiceInput } from './dto/update-invoice.input';
import {PaginationArgs, SearchArgs} from "../common/dto/args/indext";


@Resolver(() => Invoice)
export class InvoiceResolver {
  constructor(private readonly invoiceService: InvoiceService) {}

  @Mutation(() => Invoice, {name: 'createInvoice'})
  createInvoice(@Args('createInvoiceInput') createInvoiceInput: CreateInvoiceInput): Promise<Invoice> {
    return this.invoiceService.create(createInvoiceInput);
  }

  @Query(() => Number, { name: 'countInvoices' })
  async getCount(): Promise<number> {
    return this.invoiceService.getCount()
  }

  @Query(() => [Invoice], { name: 'invoices' })
  findAll(
      @Args() paginationArgs: PaginationArgs,
      @Args() searchArgs: SearchArgs): Promise<Invoice[]> {
    return this.invoiceService.findAll(paginationArgs, searchArgs);
  }

  @Query(() => Invoice, { name: 'invoice' })
  findOne(@Args('id', { type: () => ID }) id: string): Promise<Invoice> {
    return this.invoiceService.findOne(id);
  }

  @Mutation(() => Invoice)
  updateInvoice(@Args('updateInvoiceInput') updateInvoiceInput: UpdateInvoiceInput): Promise<Invoice> {
    return this.invoiceService.update(updateInvoiceInput.id, updateInvoiceInput);
  }

  @Mutation(() => Invoice)
  removeInvoice(@Args('id', { type: () => ID }) id: string) {
    return this.invoiceService.remove(id);
  }

}
