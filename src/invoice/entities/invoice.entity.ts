import {ObjectType, Field, Int, ID} from '@nestjs/graphql';
import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity({name: 'invoices'})
@ObjectType()
export class Invoice {
  @PrimaryGeneratedColumn('uuid')
  @Field(() => ID)
  id: string;

  @Column()
  @Field(() => String)
  fullName: string

  @Column()
  @Field(() => String)
  codeRequest: string

  @Column()
  @Field(() => String)
  typeDocument: string

  @Column()
  @Field(() => Int)
  numberDocument: number

  @Column()
  @Field(() => String)
  dateRequest: string

  @Column()
  @Field(() => String)
  address: string

  @Column()
  @Field(() => Boolean)
  status: boolean

}
