import {BadRequestException, Injectable, InternalServerErrorException, Logger, NotFoundException} from '@nestjs/common';
import {CreateInvoiceInput} from './dto/create-invoice.input';
import {UpdateInvoiceInput} from './dto/update-invoice.input';
import {Invoice} from "./entities/invoice.entity";
import {Repository} from "typeorm";
import {InjectRepository} from "@nestjs/typeorm";
import {PaginationArgs, SearchArgs} from "../common/dto/args/indext";

@Injectable()
export class InvoiceService {
    private logger: Logger = new Logger('InvoiceService');

    constructor(
        @InjectRepository(Invoice)
        private readonly invoiceRepository: Repository<Invoice>) {
    }


    async create(createInvoiceInput: CreateInvoiceInput): Promise<Invoice> {
        try {
            const newInvoice = this.invoiceRepository.create({
                ...createInvoiceInput
            });
            return await this.invoiceRepository.save(newInvoice);
        } catch (error) {
            this.handleDBErrors(error);
        }
    }

    async getCount(): Promise<number> {
        const count = await this.invoiceRepository.count();
        return count
    }

    findAll(paginationArgs: PaginationArgs, searchArgs: SearchArgs): Promise<Invoice[]> {
        const {limit, offset} = paginationArgs;
        const {search} = searchArgs;
        const queryBuilder = this.invoiceRepository.createQueryBuilder("Invoice")
            .take(limit).skip((limit * offset) - limit);

        if (search?.length > 0) {
            if (search[0].trim() != "") {
                queryBuilder.where('Invoice.codeRequest like :name', {name: `%${search[0]}%`});
            }else if (search[1].trim() != "") {
                queryBuilder.orWhere('Invoice.typeDocument like :name', {name: `%${search[1]}%`});
            }else if(search[2].trim() != "") {
                let numberDocument = Number(search[2]);
                queryBuilder.orWhere('Invoice.numberDocument = :name', {name: numberDocument});
            }else{
                console.log("no found")
                throw new NotFoundException();
            }
        }
        return queryBuilder.getMany();
    }

    async findOne(id: string) {
        const item = await this.invoiceRepository.findOneBy({id});
        if (!item) throw new NotFoundException(`Item with id: {id} no found`);
        return item;
    }

    async update(id: string, updateInvoiceInput: UpdateInvoiceInput) {
        try {
            const user = await this.invoiceRepository.preload({
                ...updateInvoiceInput,
                id
            });
            return await this.invoiceRepository.save(user)
        } catch (error) {
            this.handleDBErrors(error)
        }

    }

    async remove(id: string) {
        const item = await this.findOne(id);
        await this.invoiceRepository.remove(item);
        return {...item, id}
    }

    private handleDBErrors(error: any): never {

        if (error.code === "23505") {
            throw new BadRequestException(error.detail.replace('Key', ''));
        }

        if (error.code === "error-001") {
            throw new BadRequestException(error.detail.replace('Key', ''));
        }

        this.logger.error(error);
        throw new InternalServerErrorException('Please check server logs...')
    }
}
