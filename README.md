## Dashfleet Graphql with Nest.js

## Install
```
cd ..
npm i
npm run start
```

Note: Modify the file name ".env.example"

## Apollo Server Url
http://localhost:3200/graphql

### Create Seed
```
mutation createSeed{
  executeSeed
}
```

### Create invoice
```
mutation createInvoice($fullName: String!, $codeRequest: String!, $typeDocument: String!, $numberDocument: Int!, $dateRequest: String!, $status: Boolean!) {
  createInvoice(createInvoiceInput:{
    fullName: $fullName,
    codeRequest: $codeRequest,
 	dateRequest: $dateRequest,
    numberDocument: $numberDocument,
    status: $status,
    typeDocument: $typeDocument,
  }){
    id
    codeRequest
    fullName

  }
}
````

### Update invoice
```
mutation updateInvoice ($id: ID!, $codeRequest: String, $typeDocument: String, $numberDocument: Int, $dateRequest: String, $status: Boolean){
  updateInvoice(updateInvoiceInput:{
    id: $id,
    codeRequest: $codeRequest,
 		dateRequest: $dateRequest,
    numberDocument: $numberDocument,
    status: $status,
    typeDocument: $typeDocument,
  }){
    id
  }
}
```

### Delete Invoice
```
mutation deleteInvoice($id: ID!){
  removeInvoice(id:$id){
    id
  }
}
```


## Get Invoices

```
    query getSearchInvoices($offset: Int, $limit: Int, $search:[String!]){
        invoices(
          offset: $offset
          limit: $limit
          search: $search
        ){
   			id
        codeRequest
        numberDocument
        typeDocument
        }
    }
```

## Get Count Invoices Results
```
query countInvoices {
  countInvoices
}
```

## Get Invoices With Search
```
query getSearchInvoices($offset: Int, $limit: Int, $search:[String!]){
        invoices(
          offset: $offset
          limit: $limit
          search: $search
        ){
   			id
        codeRequest
        numberDocument
        typeDocument
        }
    }
```
